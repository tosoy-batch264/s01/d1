<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    // Action to return a view containing a form for blog post creation.
    public function create() 
    {
        return view('posts.create');
    }
}
